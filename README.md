# Sample docker configuration for develoment node.js
- Run this command to build docker container
```
docker-compose build
```
- Run this command to start develop
```
docker-compose up
```
- This docker image use `nodemon` to watch changes and restart node.js application in development node